// tracecmdjava.i
%module CTraceCmd
%include "typemaps.i"
%include "constraints.i"
%include "various.i"

%apply Pointer NONNULL { struct tracecmd_input *handle };
%apply Pointer NONNULL { struct tep_handle *pevent };
%apply Pointer NONNULL { struct tep_format_field * };
%apply unsigned long long *OUTPUT {unsigned long long *}
%apply unsigned long *OUTPUT {unsigned long *}
%apply char *BYTE { char *buf };
%apply int *OUTPUT {int *}

%{
#include "trace-cmd.h"
#include "event-parse.h"
#include "event-utils.h"
%}

%inline %{
struct tracecmd_input *tracecmd_alloc(const char *file, int flags);
int tracecmd_read_headers(struct tracecmd_input *handle, int state);
int tracecmd_cpus(struct tracecmd_input *handle);
int tracecmd_long_size(struct tracecmd_input *handle);
struct tep_record *
tracecmd_read_next_data(struct tracecmd_input *handle, int *rec_cpu);
struct tep_record *
tracecmd_peek_data(struct tracecmd_input *handle, int cpu);
void tracecmd_set_all_cpus_to_timestamp(struct tracecmd_input *handle,
					unsigned long long time);

unsigned long long tracecmd_record_ts_get(struct tep_record *record)
{
	return record->ts;
}

int tracecmd_record_cpu_get(struct tep_record *record)
{
	return record->cpu;
}

const char *tracecmd_event_name_get(const struct tep_event *event)
{
	return event->name;
}

const char *tracecmd_event_system_get(const struct tep_event *event)
{
	return event->system;
}

static inline struct tep_record *
tracecmd_peek_data_ref(struct tracecmd_input *handle, int cpu)
{
	struct tep_record *rec = tracecmd_peek_data(handle, cpu);
	if (rec)
		rec->ref_count++;
	return rec;
}

char *java_field_get_str(struct tep_format_field *f, struct tep_record *r)
{
	if (!strncmp(f->type, "__data_loc ", 11)) {
		unsigned long long val;
		int offset;

		if (tep_read_number_field(f, r->data, &val))
			return NULL;

		/*
		 * The actual length of the dynamic array is stored
		 * in the top half of the field, and the offset
		 * is in the bottom half of the 32 bit field.
		 */
		offset = val & 0xffff;
		return r->data + offset;
	}

	return r->data + f->offset;
}

int java_field_get_data(struct tep_format_field *f, struct tep_record *r,
			char *buf, size_t len)
{
	unsigned long long val;
	int size, offset;

	if (!strncmp(f->type, "__data_loc ", 11)) {
		if (tep_read_number_field(f, r->data, &val))
			return -1;

		/*
		 * The actual length of the dynamic array is stored
		 * in the top half of the field, and the offset
		 * is in the bottom half of the 32 bit field.
		 */
		offset = val & 0xffff;
		size = val >> 16;
	} else {
		offset = f->offset;
		size = f->size;
	}

	if (len < size)
		return size;

	memcpy(buf, r->data + offset, len);
	return len;
}

static void *java_field_get_stack_init(struct tep_record *record,
				       struct tep_event *event)
{
	struct tep_format_field *field;
	void *data = record->data;

	field = tep_find_any_field(event, "caller");
	if (!field)
		return NULL;

	return data + field->offset;
}

static int java_field_get_stack_check(struct tep_record *record,
				      struct tep_event *event,
				      void *data, int long_size,
				      unsigned long *addr)
{
	int rv;

	rv = data < record->data + record->size;
	if (!rv)
		return 1;

	*addr = tep_read_number(event->tep, data, long_size);
	rv = ((long_size == 8 && *addr == (unsigned long long)-1) ||
	      ((int)*addr == -1));

	return rv;
}

static void *java_field_get_stack_next(void *data, int long_size)
{
	return data + long_size;
}

static const char *java_field_get_stack_str(struct tep_event *event,
					    unsigned long addr)
{
	return tep_find_function(event->tep, addr);
}

struct tep_format_field *tracecmd_event_format_fields_get(struct tep_event *event)
{
	return event->format.fields;
}

struct tep_format_field *tracecmd_field_next_get(struct tep_format_field *f)
{
	return f->next;
}

char *tracecmd_field_name_get(struct tep_format_field *f)
{
	return f->name;
}
%}

%ignore trace_seq_vprintf;
%ignore vpr_stat;

/* SWIG can't grok these, define them to nothing */
#define __trace
#define __attribute__(x)
#define __thread

%include "trace-cmd.h"
%include <trace-seq.h>
%include <event-parse.h>
