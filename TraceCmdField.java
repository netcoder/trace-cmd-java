// SPDX-License-Identifier: LGPL-2.1
/*
 * Copyright (C) 2022 Red Hat Inc, Alexander Aring <aahringo@redhat.com>
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

package tracecmd;

import java.math.BigInteger;

import tracecmd.swig.*;

/**
* TraceCmdEventField represent a field of an trace event.
*
* @author Alexander Aring <aahringo@redhat.com>
*
*/
public class TraceCmdField
{
	private SWIGTYPE_p_tep_format_field field;
	private SWIGTYPE_p_tep_record record;

	private Byte[] data;

	public TraceCmdField(SWIGTYPE_p_tep_record record,
			     SWIGTYPE_p_tep_format_field field)
	{
		this.record = record;
		this.field = field;
	}

	/**
	 * Get the field data as java byte array.
	 *
	 * @return the field data as byte array.
	 */
	public Byte[] getData() throws TraceCmdException
	{
		byte ret[];
		int rv, i;

		if (this.data != null)
			return this.data;

		/* get the length at first */
		rv = CTraceCmd.java_field_get_data(this.field, this.record,
						   new byte[0], 0);
		if (rv < 0)
			throw new TraceCmdException("Failed to get field data length, rv: " + rv);

		ret = new byte[rv];
		for (byte b:ret) {
			b = 0;
		}

		rv = CTraceCmd.java_field_get_data(this.field, this.record,
						   ret, rv);
		if (rv < 0)
			throw new TraceCmdException("Failed to get field data, rv: " + rv);

		Byte[] byteObjects = new Byte[ret.length];
		i = 0;
		for (byte b:ret)
			byteObjects[i++] = b;

		this.data = byteObjects;
		return this.data;
	}

	/**
	 * Get the field data as BigInteger.
	 *
	 * @return the field data as BigInteger.
	 */
	public BigInteger getNum() throws TraceCmdException
	{
		BigInteger[] val = { new BigInteger("0") };
		SWIGTYPE_p_void rd;

		rd = CTraceCmd.tep_record_data_get(this.record);
		if (rd == null)
			throw new TraceCmdException("Failed to retrieve field data");

		int rv = CTraceCmd.tep_read_number_field(this.field, rd, val);
		if (rv != 0)
			throw new TraceCmdException("Failed to read field number, rv: " + rv);

		return val[0];
	}

	@Override
	public String toString()
	{
		String ret;

		ret = CTraceCmd.java_field_get_str(this.field, this.record);
		if (ret == null)
			throw new TraceCmdException("Failed to retrieve field string");

		return ret;
	}
}
