# trace-cmd-java

trace-cmd java bindings

Will create a libctracecmdjava.so and tracecmd.jar to parse Linux trace files in Java.
The libctracecmdjava.so is linked statically against the submodules libraries libtraceevent, libtracefs, libtracecmd which offers a independent operation besides the system installed libraries.

# Getting started

Call "make" to compile and "make DESTDIR=... prefix=... install" to install it.
