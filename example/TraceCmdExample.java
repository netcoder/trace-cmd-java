// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright (C) 2022 Red Hat Inc, Alexander Aring <aahringo@redhat.com>
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

import tracecmd.*;

/**
* Very small example how to use libtracecmd java bindings.
*
* Run as:
*
* $JAVA_HOME/bin/java -cp ../tracecmd.jar:. TraceCmdExample $TRACE.DAT
*
* @author Alexander Aring <aahringo@redhat.com>
*
*/
public class TraceCmdExample
{
	public static final void main(String[] args)
	{
		TraceCmd t = new TraceCmd(args[0]);

		System.out.println("First Timestamp: " + t.getFirstTs());
		System.out.println("Amount of Cpus: " + t.getCpus());

		for (TraceCmdEvent e:t) {
			System.out.println(e);
		}
	}
}
