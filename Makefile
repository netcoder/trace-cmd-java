# SPDX-License-Identifier: GPL-2.0

TRACECMDJAVA_VERSION	:= 0.0.2
PWD			:= $(shell pwd)
prefix			?= /usr
DESTDIR			?=
JAVA_HOME		?= /usr/lib/jvm/java

INSTALL = install

JAVA_JAVAC = $(JAVA_HOME)/bin/javac
JAVA_JAR = $(JAVA_HOME)/bin/jar
JAVA_INCLUDES = -I$(JAVA_HOME)/include -I$(JAVA_HOME)/include/linux

LP64 := $(shell echo __LP64__ | ${CC} ${CFLAGS} -E -x c - | tail -n 1)
ifeq ($(LP64), 1)
	libdir_relative = lib64
else
	libdir_relative = lib
endif

libdir = $(prefix)/$(libdir_relative)
datadir = $(prefix)/share
includedir = $(prefix)/include
javadir = $(datadir)/java

thirdpartydir		:= third_party
thirdpartytmpdir	:= $(thirdpartydir)/tmp
builddir		:= build
prefixdir		:= $(builddir)/prefix

zlibversion		:= 1.2.12
zlibtar			:= $(thirdpartydir)/zlib-$(zlibversion).tar.gz
zlibdir			:= $(thirdpartytmpdir)/zlib-$(zlibversion)
libz			:= $(prefixdir)/$(libdir)/libz.a

zstdversion		:= 1.5.2
zstdtar			:= $(thirdpartydir)/zstd-$(zstdversion).tar.gz
zstddir			:= $(thirdpartytmpdir)/zstd-$(zstdversion)
libzstd			:= $(prefixdir)/$(libdir)/libzstd.a

libtraceeventversion	:= 1.6.1
libtraceeventtar	:= $(thirdpartydir)/libtraceevent-$(libtraceeventversion).tar.gz
libtraceeventdir	:= $(thirdpartytmpdir)/libtraceevent-$(libtraceeventversion)
libtraceevent		:= $(prefixdir)/$(libdir)/libtraceevent.a

libtracefsversion	:= 1.4.1
libtracefstar		:= $(thirdpartydir)/libtracefs-$(libtracefsversion).tar.gz
libtracefsdir		:= $(thirdpartytmpdir)/libtracefs-$(libtracefsversion)
libtracefs		:= $(prefixdir)/$(libdir)/libtracefs.a

tracecmdversion		:= v3.1.1
tracecmdtar		:= $(thirdpartydir)/trace-cmd-$(tracecmdversion).tar.gz
tracecmddir		:= $(thirdpartytmpdir)/trace-cmd-$(tracecmdversion)
libtracecmd		:= $(tracecmddir)/lib/trace-cmd/libtracecmd.a

pkgprefixdir = $(PWD)/$(prefixdir)
pkgconfigdir = $(pkgprefixdir)/$(libdir)/pkgconfig
libtraceeventpc = $(pkgconfigdir)/libtraceevent.pc
libtracefspc = $(pkgconfigdir)/libtracefs.pc

installprefixdir = $(PWD)/$(prefixdir)/$(prefix)

export PKG_CONFIG_PATH=$(pkgconfigdir)

SRCS := TraceCmd.java TraceCmdEvent.java TraceCmdException.java TraceCmdField.java
root_pkgdir := tracecmd
swigdir := swig

all: jars

jars: tracecmd-$(TRACECMDJAVA_VERSION).jar
	ln -s tracecmd-$(TRACECMDJAVA_VERSION).jar tracecmd.jar

install: libctracecmdjava-$(TRACECMDJAVA_VERSION).so tracecmd-$(TRACECMDJAVA_VERSION).jar force
	mkdir -p $(DESTDIR)/$(libdir) $(DESTDIR)/$(javadir)
	$(INSTALL) -m 755 libctracecmdjava-$(TRACECMDJAVA_VERSION).so '$(DESTDIR)/$(libdir)'
	ln -sf libctracecmdjava-$(TRACECMDJAVA_VERSION).so '$(DESTDIR)/$(libdir)/libctracecmdjava.so'
	$(INSTALL) -m 644 tracecmd-$(TRACECMDJAVA_VERSION).jar '$(DESTDIR)/$(javadir)'
	ln -sf tracecmd-$(TRACECMDJAVA_VERSION).jar $(DESTDIR)/$(javadir)/tracecmd.jar

$(zlibdir):
	mkdir -p $(thirdpartytmpdir)
	tar xf $(zlibtar) -C $(thirdpartytmpdir)

$(libz): $(zlibdir)
	@echo "make $(libz)"
	mkdir -p $(prefixdir)
	cd $(zlibdir) && ./configure --prefix=${prefix} --libdir=${libdir}
	make -C $(zlibdir) DESTDIR=$(PWD)/$(prefixdir) libz.a
	mkdir -p $(PWD)/$(prefixdir)/$(includedir)
	cp $(zlibdir)/zlib.h $(PWD)/$(prefixdir)/$(includedir)
	cp $(zlibdir)/zconf.h $(PWD)/$(prefixdir)/$(includedir)
	mkdir -p $(PWD)/$(prefixdir)/$(libdir)
	cp $(zlibdir)/libz.a $(PWD)/$(prefixdir)/$(libdir)

$(zstddir):
	mkdir -p $(thirdpartytmpdir)
	tar xf $(zstdtar) -C $(thirdpartytmpdir)

$(libzstd): $(zstddir)
	@echo "make $(libzstd)"
	mkdir -p $(prefixdir)
	make -C $(zstddir) prefix=$(PWD)/$(prefixdir) libdir='$${prefix}/$(libdir)' includedir='$${prefix}/$(includedir)'
	mkdir -p $(pkgconfigdir)
	cp $(zstddir)/lib/libzstd.pc $(pkgconfigdir)
	mkdir -p $(PWD)/$(prefixdir)/$(includedir)
	cp $(zstddir)/lib/*.h $(PWD)/$(prefixdir)/$(includedir)
	mkdir -p $(PWD)/$(prefixdir)/$(libdir)
	cp $(zstddir)/lib/libzstd.a $(PWD)/$(prefixdir)/$(libdir)

$(libtraceeventdir):
	mkdir -p $(thirdpartytmpdir)
	tar xf $(libtraceeventtar) -C $(thirdpartytmpdir)

$(libtraceevent): $(libtraceeventdir)
	@echo "make $(libtraceevent)"
	mkdir -p $(prefixdir)
	make -C $(libtraceeventdir) prefix=${prefix} pkgconfig_dir=$(libdir)/pkgconfig DESTDIR=$(PWD)/$(prefixdir) install
	sed -i '1s|^.*$$|prefix=$(installprefixdir)|' $(libtraceeventpc)

$(libtracefsdir):
	mkdir -p $(thirdpartytmpdir)
	tar xf $(libtracefstar) -C $(thirdpartytmpdir)

$(libtracefs): $(libtracefsdir) $(libtraceevent)
	@echo "make $(libtracefs)"
	mkdir -p $(prefixdir)
	make -C $(libtracefsdir) prefix=${prefix} pkgconfig_dir=$(libdir)/pkgconfig DESTDIR=$(PWD)/$(prefixdir) install
	sed -i '1s|^.*$$|prefix=$(installprefixdir)|' $(libtracefspc)

$(tracecmddir):
	mkdir -p $(thirdpartytmpdir)
	tar xf $(tracecmdtar) -C $(thirdpartytmpdir)

$(libtracecmd): $(libz) $(libzstd) $(tracecmddir) $(libtraceevent) $(libtracefs)
	@echo "make $(libtracecmd)"
	mkdir -p $(prefixdir)
	make -C $(tracecmddir) V=1 CC="$(CC) -I $(PWD)/$(prefixdir)/$(includedir)" TEST_LIBZSTD="n" ZLIB_INSTALLED=1 prefix=${prefix} DESTDIR=$(PWD)/$(prefixdir) install

libctracecmdjava-$(TRACECMDJAVA_VERSION).so: ctracecmdjava.i $(libtracecmd) $(libtraceevent) $(libtracefs)
	@mkdir -p $(swigdir)
	swig -Wall -java -noproxy -package $(root_pkgdir).swig -I$(tracecmddir)/include/trace-cmd `pkg-config --cflags libtraceevent libtracefs` -outdir $(swigdir) ctracecmdjava.i
	$(CC) $(CFLAGS) -fPIC -c -I$(PWD)/$(prefixdir)/$(includedir) $(JAVA_INCLUDES) -I$(tracecmddir)/include/trace-cmd `pkg-config --cflags libtraceevent libtracefs` ctracecmdjava_wrap.c
	$(CC) --shared -Wl,-soname,libctracecmdjava-$(TRACECMDJAVA_VERSION).so -o libctracecmdjava-$(TRACECMDJAVA_VERSION).so ctracecmdjava_wrap.o $(libz) $(libtracecmd) $(libtracefs) $(libtraceevent)
	ln -sf libctracecmdjava-$(TRACECMDJAVA_VERSION).so libctracecmdjava.so

tracecmd-$(TRACECMDJAVA_VERSION).jar: libctracecmdjava-$(TRACECMDJAVA_VERSION).so $(SRCS)
	$(JAVA_JAVAC) -d . $(swigdir)/*.java *.java
	$(JAVA_JAR) cf tracecmd-$(TRACECMDJAVA_VERSION).jar $(root_pkgdir)

clean:
	$(RM) *.jar *.a *.so *.o .*.d ctracecmdjava_wrap.*
	$(RM) -r $(swigdir) tracecmd $(builddir) $(thirdpartytmpdir)

force:
.PHONY: clean force jars
