// SPDX-License-Identifier: LGPL-2.1
/*
 * Copyright (C) 2022 Red Hat Inc, Alexander Aring <aahringo@redhat.com>
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

package tracecmd;

class TraceCmdException extends RuntimeException
{
	public TraceCmdException(String m)
	{
		super(m);
	}
}
