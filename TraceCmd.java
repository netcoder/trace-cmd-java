// SPDX-License-Identifier: LGPL-2.1
/*
 * Copyright (C) 2022 Red Hat Inc, Alexander Aring <aahringo@redhat.com>
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

package tracecmd;

import java.math.BigInteger;
import java.util.LinkedList;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import tracecmd.swig.*;

/**
* Represents a trace instance of a trace file.
*
* @author Alexander Aring <aahringo@redhat.com>
*
*/
public class TraceCmd implements Iterable<TraceCmdEvent>, AutoCloseable
{
	static { System.loadLibrary("ctracecmdjava"); }

	private SWIGTYPE_p_tracecmd_input handle;
	private SWIGTYPE_p_tep_handle pevent;
	private List<TraceCmdEvent> events;
	private BigInteger first_ts;
	private Integer long_size;
	private Integer cpus;

	/**
	 * Instantiate a TraceCmd Object to read a tracefile
	 *
	 * @param file relative or absolute filepath to the tracefile to read
	 */
	public TraceCmd(String file)
	{
		int rv;

		this.handle = CTraceCmd.tracecmd_alloc(file, 0);
		if (this.handle == null)
			throw new TraceCmdException("Failed to alloc file: " + file);

		/* 0 is TRACECMD_FILE_ALLOCATED */
		rv = CTraceCmd.tracecmd_read_headers(this.handle, 0);
		if (rv != 0)
			throw new TraceCmdException("Failed to read headers of file: " + file);

		rv = CTraceCmd.tracecmd_init_data(this.handle);
		if (rv != 0)
			throw new TraceCmdException("Failed to init data of file: " + file);

		this.pevent = CTraceCmd.tracecmd_get_tep(this.handle);
	}

	/**
	 * Get the number of CPUs recorded
	 *
	 * @return the number of CPUs recorded
	 */
	public Integer getCpus()
	{
		if (this.cpus != null)
			return this.cpus;

		this.cpus = CTraceCmd.tracecmd_cpus(this.handle);
		return this.cpus;
	}

	/**
	 * Get the size of a long for the recorded tracefile
	 *
	 * @return the size of a long type
	 */
	public Integer getLongSize()
	{
		if (this.long_size != null)
			return this.long_size;

		this.long_size = CTraceCmd.tracecmd_long_size(this.handle);
		return this.long_size;
	}

	/**
	 * Get the first recorded timestamp
	 *
	 * @return the first recorded timestamp
	 */
	public BigInteger getFirstTs()
	{
		if (this.first_ts != null)
			return this.first_ts;

		this.first_ts = CTraceCmd.tracecmd_get_first_ts(this.handle);
		return this.first_ts;
	}

	/**
	 * Read the next record and increment
	 *
	 * Returns null if end reached.
	 *
	 * @param cpu the CPU to pull from
	 * @return the specifc event as a TraceCmdEvent object
	 */
	public TraceCmdEvent getEvent(Integer cpu)
	{
		SWIGTYPE_p_tep_event format;
		SWIGTYPE_p_tep_record rec;
		int type;

		rec = CTraceCmd.tracecmd_read_data(this.handle, cpu);
		if (rec == null)
			return null;

		type = CTraceCmd.tep_data_type(this.pevent, rec);
		format = CTraceCmd.tep_find_event(this.pevent, type);

		return new TraceCmdEvent(this.pevent, rec, format);
	}

	/**
	 * Read a record from a specific offset
	 *
	 * @param offset the offset into the file to find the record
	 * @return the specifc event as a TraceCmdEvent object
	 */
	public TraceCmdEvent getEventAt(BigInteger offset) throws TraceCmdException
	{
		SWIGTYPE_p_tep_event format;
		SWIGTYPE_p_tep_record rec;
		int type;

		rec = CTraceCmd.tracecmd_read_at(this.handle, offset, null);
		if (rec == null)
			throw new TraceCmdException("Failed to read data at: " + offset);

		type = CTraceCmd.tep_data_type(this.pevent, rec);
		format = CTraceCmd.tep_find_event(this.pevent, type);

		return new TraceCmdEvent(this.pevent, rec, format);
	}

	/**
	 * Read the next record on any cpu.
	 *
	 * Returns null if end reached.
	 *
	 * @return the specifc event as a TraceCmdEvent object
	 */
	public TraceCmdEvent getNextEvent()
	{
		SWIGTYPE_p_tep_event format;
		SWIGTYPE_p_tep_record rec;
		int[] rec_cpu = { 0 };
		int type;

		rec = CTraceCmd.tracecmd_read_next_data(this.handle, rec_cpu);
		if (rec == null)
			return null;

		type = CTraceCmd.tep_data_type(this.pevent, rec);
		format = CTraceCmd.tep_find_event(this.pevent, type);

		return new TraceCmdEvent(this.pevent, rec, format);
	}


	/**
	 * Return the record at the current location by cpu iterator.
	 *
	 * Returns null if end reached.
	 *
	 * @param cpu the CPU to pull from
	 * @return the specifc event as a TraceCmdEvent object
	 */
	public TraceCmdEvent peekEvent(Integer cpu)
	{
		SWIGTYPE_p_tep_event format;
		SWIGTYPE_p_tep_record rec;
		int type;

		rec = CTraceCmd.tracecmd_peek_data_ref(this.handle, cpu);
		if (rec == null)
			return null;

		type = CTraceCmd.tep_data_type(this.pevent, rec);
		format = CTraceCmd.tep_find_event(this.pevent, type);

		return new TraceCmdEvent(this.pevent, rec, format);
	}

	/**
	 * Closes the TraceCmd handle.
	 *
	 * If not called AutoCloseable will take care about it.
	 */
	@Override
	public void close()
	{
		CTraceCmd.tracecmd_close(this.handle);
	}

	/**
	 * Iterator over all Events by using getNextEvent().
	 *
	 * Probably the best and easiest way to multiple times iterate
	 * over all events in order of their timestamp. Note that all
	 * events will be held in memory of TraceCmd lifetime.
	 *
	 * Just use for-each loop e.g. for (TraceCmdEvent e:new TraceCmd(...))
	 *
	 * @return iterator object
	 */
	@Override
	public Iterator<TraceCmdEvent> iterator()
	{
		if (this.events == null) {
			TraceCmdEvent e;

			CTraceCmd.tracecmd_set_all_cpus_to_timestamp(this.handle, this.getFirstTs());
			this.events = new LinkedList<TraceCmdEvent>();
			e = this.getNextEvent();
			while (e != null) {
				this.events.add(e);
				e = this.getNextEvent();
			}
		}

		return this.events.listIterator();
	}
}
