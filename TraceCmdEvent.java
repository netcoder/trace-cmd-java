// SPDX-License-Identifier: LGPL-2.1
/*
 * Copyright (C) 2022 Red Hat Inc, Alexander Aring <aahringo@redhat.com>
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

package tracecmd;

import java.lang.ref.Cleaner;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import tracecmd.swig.*;

/**
* TraceCmdEvent represent a trace event.
*
* @author Alexander Aring <aahringo@redhat.com>
*
*/
public class TraceCmdEvent implements Comparable<TraceCmdEvent>, AutoCloseable
{
	/* memory free handler */
	private static final Cleaner cleaner = Cleaner.create();
	private final Cleaner.Cleanable cleanable;
	private final TraceCmdEventCleaner ec;

	/* swig handlers */
	private SWIGTYPE_p_tep_handle pevent;
	private SWIGTYPE_p_tep_record record;
	private SWIGTYPE_p_tep_event format;

	/* caches attributes */
	private Set<String> keys;
	private BigInteger ts;
	private String system;
	private Integer type;
	private String name;
	private Integer cpu;
	private String comm;
	private Integer pid;

	static class TraceCmdEventCleaner implements Runnable {
		private SWIGTYPE_p_tep_handle pevent;

		public TraceCmdEventCleaner(SWIGTYPE_p_tep_handle pevent) {
			CTraceCmd.tep_ref(pevent);
			this.pevent = pevent;
		}

		@Override
		public void run(){
			CTraceCmd.tep_unref(this.pevent);
		}
	}

	public TraceCmdEvent(SWIGTYPE_p_tep_handle pevent,
			     SWIGTYPE_p_tep_record record,
			     SWIGTYPE_p_tep_event format)
	{
		this.pevent = pevent;
		this.record = record;
		this.format = format;

		this.ec = new TraceCmdEventCleaner(pevent);
		this.cleanable = cleaner.register(this, this.ec);
		CTraceCmd.tep_unref(pevent);
	}

	public TraceCmdEvent(TraceCmdEvent e)
	{
		this.pevent = e.pevent;
		this.record = e.record;
		this.format = e.format;

		this.ec = new TraceCmdEventCleaner(e.pevent);
		this.cleanable = cleaner.register(this, this.ec);
	}

	/**
	 * Freeing up a TraceCmdEvent object.
	 *
	 * If not called AutoCloseable will take care about it.
	 */
	@Override
	public void close()
	{
		cleanable.clean();
	}

	/**
	 * Get the trace system name of the trace event.
	 *
	 * @return the specific trace system as string
	 */
	public String getSystem()
	{
		if (this.system != null)
			return this.system;

		this.system = CTraceCmd.tracecmd_event_system_get(this.format);
		return this.system;
	}

	/**
	 * Get the trace event name.
	 *
	 * @return the specific trace event name as string
	 */
	public String getName()
	{
		if (this.name != null)
			return this.name;

		this.name = CTraceCmd.tracecmd_event_name_get(this.format);
		return this.name;
	}

	/**
	 * Get the trace event timestamp.
	 *
	 * @return the specific trace event timestamp in uptime nanoseconds.
	 */
	public BigInteger getTs()
	{
		if (this.ts != null)
			return this.ts;

		this.ts = CTraceCmd.tracecmd_record_ts_get(this.record);
		return this.ts;
	}

	/**
	 * Get the cpu on which the event was recorded on.
	 *
	 * @return the specific cpu which the event was record on.
	 */
	public Integer getCpu()
	{
		if (this.cpu != null)
			return this.cpu;

		this.cpu = CTraceCmd.tracecmd_record_cpu_get(this.record);
		return this.cpu;
	}

	/**
	 * Get the pid on which the event was recorded on.
	 *
	 * @return the specific pid which the event was record on.
	 */
	public Integer getPid()
	{
		if (this.pid != null)
			return this.pid;

		this.pid = CTraceCmd.tep_data_pid(this.pevent, this.record);
		return this.pid;
	}

	public Integer getType()
	{
		if (this.type != null)
			return this.type;

		this.type = CTraceCmd.tep_data_type(this.pevent, this.record);
		return this.type;
	}

	public String getComm()
	{
		if (this.comm != null)
			return this.comm;

		this.comm = CTraceCmd.tep_data_comm_from_pid(this.pevent, this.getPid());
		return this.comm;
	}

	/**
	 * Get a set of available keys for getField().
	 *
	 * @return a String set for specific keys.
	 */
	public Set<String> getKeys()
	{
		HashSet<String> ret = new HashSet<String>();
		SWIGTYPE_p_tep_format_field f;

		if (this.keys != null)
			return this.keys;

		f = CTraceCmd.tracecmd_event_format_fields_get(this.format);
		while (f != null) {
			ret.add(CTraceCmd.tracecmd_field_name_get(f));
			f = CTraceCmd.tracecmd_field_next_get(f);
		}

		this.keys = ret;
		return this.keys;
	}

	public List<String> getStack(Integer long_size) throws TraceCmdException
	{
		List<String> ret = new ArrayList<String>();
		long[] addr = { 0 };
		SWIGTYPE_p_void i;
		String str;
		int rv;

		i = CTraceCmd.java_field_get_stack_init(this.record, this.format);
		if (i == null)
			throw new TraceCmdException("Failed to get stack data");

		for (;;) {
			rv = CTraceCmd.java_field_get_stack_check(this.record,
								  this.format,
								  i, long_size,
								  addr);
			if (rv == 1)
				break;

			str = CTraceCmd.java_field_get_stack_str(this.format, addr[0]);
			if (str == null)
				throw new TraceCmdException("Failed to get stack string");

			ret.add(str);
			i = CTraceCmd.java_field_get_stack_next(i, long_size);
		}

		return ret;
	}

	/**
	 * Get a TraceCmdField object for a specific trace field.
	 *
	 * @param name the name of the field.
	 * @return a TraceCmdField object which represent the event field.
	 */
	public TraceCmdField getField(String name) throws TraceCmdException
	{
		SWIGTYPE_p_tep_format_field f = CTraceCmd.tep_find_field(this.format, name);
		if (f == null)
			throw new TraceCmdException("Field not found: " + name);

		return new TraceCmdField(this.record, f);
	}

	/**
	 * Get a number value of a specific trace field.
	 *
	 * @param name the name of the field.
	 * @return the number value.
	 */
	public BigInteger getNumField(String name)
	{
		return this.getField(name).getNum();
	}

	/**
	 * Get a string value of a specific trace field.
	 *
	 * @param name the name of the field.
	 * @return the string value.
	 */
	public String getStrField(String name)
	{
		return this.getField(name).toString();
	}

	/**
	 * Get a string representation of the trace event.
	 *
	 * @param name the name of the field.
	 * @return the string representation.
	 */
	@Override
	public String toString()
	{
		BigInteger[] ts = this.getTs().divideAndRemainder(new BigInteger("1000000000"));

		return String.format("%d.%09d CPU%d %s: pid=%d comm=%s type=%d",
				     ts[0], ts[1], this.getCpu(), this.getName(),
				     this.getPid(), this.getComm(), this.getType());
	}

	@Override
	public int compareTo(TraceCmdEvent e)
	{
		return this.getTs().compareTo(e.getTs());
	}
}
